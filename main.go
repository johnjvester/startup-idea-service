package main

import (
	_ "github.com/dimiro1/banner/autoload"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
)

func main() {
	router := gin.Default()
	router.GET("/", getHelloWorld)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	if err := router.Run(":" + port); err != nil {
		log.Panicf("error: %s", err)
	}
}

func getHelloWorld(c *gin.Context) {
	c.String(http.StatusOK, "Hello World")
}
