# `startup-idea-service` Repository

> The `startup-idea-service` repository is a very simple RESTful API written in the [Go Programming Language](https://go.dev/) 
> and using the [Gin Web Framework](https://gin-gonic.com/docs/). 

## Publications

This repository is related to a DZone.com publication:

* TBD

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
